//
//  ViewController.h
//  tableView
//
//  Created by Click Labs134 on 9/28/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
<UITableViewDelegate,UITableViewDataSource>


@end

