//
//  ViewController.m
//  tableView
//
//  Created by Click Labs134 on 9/28/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITableView *cellData;

@end

@implementation ViewController
@synthesize cellData;
NSArray *cellArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    [cellData reloadData ];
    // Do any additional setup after loading the view, typically from a nib.
cellArray =@[@"Suchismita",@"Sabyasachi",@"Sujata",@"Gagan"];
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cellArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.textLabel.text=cellArray[indexPath.row];
    cell.textColor=[UIColor blueColor];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
